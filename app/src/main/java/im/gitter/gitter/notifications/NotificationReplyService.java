package im.gitter.gitter.notifications;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.RemoteInput;
import android.util.Log;

import im.gitter.gitter.content.RestResponseReceiver;
import im.gitter.gitter.content.RestServiceHelper;

import static im.gitter.gitter.notifications.RoomNotifications.REMOTE_INPUT_REPLY_KEY;
import static im.gitter.gitter.notifications.RoomNotifications.ROOM_ID_INTENT_KEY;

public class NotificationReplyService extends Service {

    private RestResponseReceiver restResponseReceiver = new RestResponseReceiver();

    @Override
    public void onCreate() {
        super.onCreate();

        registerReceiver(restResponseReceiver, restResponseReceiver.getFilter());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // we dont bind to activities
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        String message = RemoteInput.getResultsFromIntent(intent).getString(REMOTE_INPUT_REPLY_KEY);
        final String roomId = intent.getStringExtra(ROOM_ID_INTENT_KEY);

        final long requestId = RestServiceHelper.getInstance().sendMessageToRoom(this, message, roomId);

        restResponseReceiver.listen(requestId, new RestResponseReceiver.Listener() {
            @Override
            public void onResponse(int statusCode) {
                // preload the room in the database
                RestServiceHelper.getInstance().getRoom(NotificationReplyService.this, roomId);

                // update the room notification to:
                // 1. clear the activity spinner
                // 2. show the message that has just been sent
                // (but also do it silently)
                new RoomNotifications(NotificationReplyService.this, roomId).updateSilently(new RoomNotifications.Listener() {
                    @Override
                    public void onFinish() {
                        stopSelf(startId);
                    }
                });
            }
        });

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterReceiver(restResponseReceiver);
    }
}
